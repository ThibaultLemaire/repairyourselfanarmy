# Mindustry Sprites

Taken from the awesome [Mindustry] game under the GPLv3 License.

[Mindustry]: https://github.com/Anuken/Mindustry/tree/master/core/assets-raw/sprites