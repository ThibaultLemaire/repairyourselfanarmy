# Repair Yourself An Army

![banner](public/banner.png)

My attempt at the Global Game Jam 2020

## Opening the source with [Godot]

This project is made with the [Godot] open-source engine. Here are the steps to open and edit it:

1. [Download and Install Godot][Download Godot]
2. [Download the source] (or clone it with [Git])
3. Start Godot
4. Go to `Import`
5. Select the `.zip` file you downloaded in 2.
6. Choose an installation folder (create it if needed)
7. Click `Import & Edit`

[Godot]: https://godotengine.org/
[Download Godot]: https://godotengine.org/download
[Download the source]: https://gitlab.com/ThibaultLemaire/repairyourselfanarmy/-/archive/master/repairyourselfanarmy-master.zip
[Git]: https://git-scm.com/

## License

This project has been initialised from Narayana Walters' [TopDownShooter] example, which is under the MIT license.
I am re-licensing this project under the GPLv3 license to grab assets from [Mindustry].

[TopDownShooter]: https://github.com/Miziziziz/TopDownShooter/blob/master/LICENSE
[Mindustry]: https://github.com/Anuken/Mindustry/blob/master/LICENSE
