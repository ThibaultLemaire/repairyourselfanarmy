extends Node2D

onready var player := $AnimationPlayer

func _ready():
	player.play("crank")

func _physics_process(_delta):
	global_rotation = 0