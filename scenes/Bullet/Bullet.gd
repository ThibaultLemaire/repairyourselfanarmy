extends RigidBody2D
class_name Bullet

export (float) var force := 0.0

func _on_Timer_timeout():
	queue_free()

func _on_Bullet_body_entered(body: Node):
	if body.has_method("on_hit"):
		body.on_hit(force)
		queue_free()
