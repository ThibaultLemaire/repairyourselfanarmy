extends KinematicBody2D

export (PackedScene) var Wrench
export (PackedScene) var Bullet

const MOVE_SPEED = 300
const REPAIR_RATE = 0.1
const BULLET_VELOCITY = 1000
const BULLET_FORCE = 0.05
const GROUP = "#ffff00"
const WRENCH_NAME = "Wrench"

var health := 1.0
var can_shoot := true

onready var headSprite: Sprite = $Sprite/Head
onready var shootPos: Position2D = $ShootPosition
onready var shootCooldown: Timer = $ShootCooldown
onready var repairTargets := Array()
onready var sprite: Sprite = $Sprite

func _ready():
	headSprite.modulate = Color(GROUP)
	add_to_group(GROUP)

func _remove_wrench(target: Node):
	var wrench := target.find_node(WRENCH_NAME, false, false)
	if wrench:
		wrench.queue_free()

func _repair(target: Node, delta: float):
	if target.health < 0.9:
		target.repair(REPAIR_RATE * delta)
		if not target.find_node(WRENCH_NAME, false, false):
			var wrench: Node = Wrench.instance()
			wrench.name = WRENCH_NAME
			target.add_child(wrench)
	else:
		_remove_wrench(target)
		if target.is_in_group("broken"):
			target.reboot(GROUP)

func _aim_and_shoot():
	var look_vec := get_global_mouse_position() - global_position
	global_rotation = look_vec.angle()
	
	if Input.is_action_pressed("shoot") and can_shoot:
		var bullet: Bullet = Bullet.instance()
		bullet.force = BULLET_FORCE
		bullet.position = shootPos.global_position
		bullet.rotation = rotation
		bullet.linear_velocity = look_vec.normalized() * BULLET_VELOCITY
		bullet.add_collision_exception_with(self)
		get_parent().add_child(bullet)
		can_shoot = false
		shootCooldown.start()

func _physics_process(delta: float):
	var move_vec = Vector2()
	if Input.is_action_pressed("move_up"):
		move_vec.y -= 1
	if Input.is_action_pressed("move_down"):
		move_vec.y += 1
	if Input.is_action_pressed("move_left"):
		move_vec.x -= 1
	if Input.is_action_pressed("move_right"):
		move_vec.x += 1
	move_vec = move_vec.normalized()
	var __ := move_and_collide(move_vec * MOVE_SPEED * delta)
	
	_aim_and_shoot()
	
	for target in repairTargets:
		_repair(target, delta)

func on_hit(force):
	health -= force
	sprite.modulate = Color(health, health, health)
	if health < 0.2:
		var __ := get_tree().reload_current_scene()

func _on_RepairField_area_entered(area: Area2D):
	repairTargets.append(area.get_parent())


func _on_RepairField_area_exited(area: Area2D):
	var target: Node = area.get_parent()
	_remove_wrench(target)
	repairTargets.erase(target)

func _on_ShootCooldown_timeout():
	can_shoot = true
