extends Path2D

signal mob_ready(instance, glob_transform)

export (float) var margin := 0.0
export (PackedScene) var Mob: PackedScene

onready var spawnLocation: PathFollow2D = $SpawnLocation

func _ready():
	randomize()

func _process(_delta):
	transform = get_viewport_transform().affine_inverse()

func draw_curve():
	var vp_rect := get_viewport_rect().grow(margin)
	var top_left := vp_rect.position
	var bottom_right := vp_rect.end
	var top_right := Vector2(bottom_right.x, top_left.y)
	var bottom_left := Vector2(top_left.x, bottom_right.y)
	curve.clear_points()
	curve.add_point(top_left)
	curve.add_point(top_right)
	curve.add_point(bottom_right)
	curve.add_point(bottom_left)
	curve.add_point(top_left)


func _on_SpawnTimer_timeout():
	spawnLocation.offset = randi()
	var rot := spawnLocation.global_rotation
	rot += PI/2 # Look towards center of screen
	rot += rand_range(-PI/4, PI/4) # More or less
	emit_signal(
		"mob_ready",
		Mob.instance(),
		Transform2D(rot, spawnLocation.global_position))
