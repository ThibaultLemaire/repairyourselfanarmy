extends KinematicBody2D

const BASE_MOVE_SPEED = 100.0
const BASE_HIT_COOLDOWN = 1.0
const BASE_HIT_FORCE = 0.2
const DECAY_RATE = 0.1
const TEAMS = ["#ff0000", "#00ff00", "#0000ff"]

onready var raycast: RayCast2D = $RayCast2D
onready var sensorEcho: Area2D = $SensorEcho
onready var cooldown: Timer = $Cooldown
onready var sprite: Sprite = $Sprite
onready var headSprite: Sprite = $Sprite/Head
onready var smoke: Particles2D = $Sprite/Smoke
onready var my_group: String = TEAMS[randi()%len(TEAMS)]

var targets := Array()
var can_hit := true
var health := 1.0
var move_speed := BASE_MOVE_SPEED
var cooldown_time := BASE_HIT_COOLDOWN
var hit_force := BASE_HIT_FORCE
var broken := false
var decay := 1.0

func _set_group():
	headSprite.modulate = Color(my_group)
	add_to_group(my_group)

func _ready():
	_set_group()

func _hit(obj: Object):
	obj.on_hit(hit_force)
	can_hit = false
	cooldown.wait_time = cooldown_time
	cooldown.start()

func _is_enemy(node: Node2D) -> bool:
	var is_broken: bool = node.is_in_group("broken")
	var is_friendly: bool = node.is_in_group(my_group)
	return not (is_friendly or is_broken)

func _is_nearer(current: Node2D, candidate: Node2D) -> bool:
	if not current:
		return true
	var pos := global_position
	var curr := current.global_position - pos
	var new := candidate.global_position - pos
	return new.length() < curr.length()

func _seek_and_destroy(delta: float):
	var nearest: Node2D = null
	for candidate in targets:
		if _is_enemy(candidate) and _is_nearer(nearest, candidate):
			nearest = candidate
	if not nearest:
		return
	var vec_to_nearest := (nearest.global_position - global_position).normalized()
	global_rotation = vec_to_nearest.angle()
	var __ := move_and_collide(vec_to_nearest * move_speed * delta)
	
	if can_hit:
		var coll: Node = raycast.get_collider()
		if coll and not coll.is_in_group(my_group):
			_hit(coll)

func _set_decay(value: float):
	decay = value
	sprite.modulate = Color(health, health, health, decay)

func _decay(amount: float):
	_set_decay(decay - amount)
	if decay < 0.1:
		queue_free()

func _physics_process(delta: float):
	if broken:
		_decay(DECAY_RATE * delta)
	else:
		_seek_and_destroy(delta)

func _adjust_stats():
	move_speed = BASE_MOVE_SPEED * health
	cooldown_time = BASE_HIT_COOLDOWN / health
	hit_force = BASE_HIT_FORCE * health

func _break():
	sensorEcho.collision_layer = 0x4
	broken = true
	add_to_group("broken")
	smoke.emitting = true
	
func reboot(new_group: String):
	_adjust_stats()
	sensorEcho.collision_layer = 0x2 + 0x4
	broken = false
	smoke.emitting = false
	remove_from_group("broken")
	remove_from_group(my_group)
	my_group = new_group
	_set_group()

func _change_health(amount: float):
	health = max(0.2, health + amount)
	sprite.modulate = Color(health, health, health)

func repair(amount: float):
	_change_health(amount)
	_set_decay(1.0)

func on_hit(force: float):
	if 0.3 < health:
		_change_health(-force)
		_adjust_stats()
	elif broken:
		_decay(force)
	else:
		_break()

func _on_Sensors_area_entered(area: Area2D):
	if area != sensorEcho:
		targets.append(area.get_parent())

func _on_Sensors_area_exited(area: Area2D):
	targets.erase(area.get_parent())

func _on_Cooldown_timeout():
	can_hit = true
