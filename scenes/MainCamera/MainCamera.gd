extends Camera2D

const SCALE_STEP := Vector2(0.1, 0.1)
const MAX_ZOOM := Vector2(1.1, 1.1)
const MIN_ZOOM := Vector2(0.2, 0.2)

func _process(_delta):
	if Input.is_action_just_released("scale_up") and MIN_ZOOM < zoom:
		zoom -= SCALE_STEP
	if Input.is_action_just_released("scale_down") and zoom < MAX_ZOOM:
		zoom += SCALE_STEP