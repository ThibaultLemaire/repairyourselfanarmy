extends Node2D

const MIN_SPAWN_TIME := 0.8

var prev := 0

onready var score := $HUD/Score
onready var player := $Player
onready var spawnTimer: Timer = $SpawnFrame/SpawnTimer

func _ready():
	randomize()

func _process(_delta: float):
	var player_army := get_tree().get_nodes_in_group(player.GROUP)
	var army_strength := len(player_army)
	if army_strength == prev:
		return
	
	score.text = str(army_strength - 1)
	spawnTimer.wait_time = MIN_SPAWN_TIME + 0.3 / army_strength
	prev = army_strength

func _on_SpawnFrame_mob_ready(instance: Node2D, glob_transform: Transform2D):
	add_child(instance)
	instance.global_transform = glob_transform
